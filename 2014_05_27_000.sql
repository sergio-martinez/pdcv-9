/*
Autor : sml
Fecha : 27/05/2014 12:21:40
Comentario : 

*/

PROMPT ************************ 2014_05_27_000 *****************************




create or replace procedure pr_nuevo_estado_anul AS 
  
	
 CURSOR c1 IS
  Select an.id_tipo_doc,an.id_ano,an.id_tipo,an.id_numero,an.id_linea,an.id_anulacion,
         an.prog_nuevo_estado,an.nuevo_estado_prog,an.fecha_nuevo_estado,an.usuario_programador,an.nueva_causa_prog, an.obs_anulacion
  from anulaciones an
    where to_char(an.fecha_nuevo_estado,'YYYYMMDD') <= to_char(sysdate,'YYYYMMDD') and
	      an.id_tipo_doc in('PV','PC') and
        an.prog_nuevo_estado = 'S' and 
        an.fecha_nuevo_estado is not null         
    order by an.id_tipo_doc,an.id_ano,an.id_tipo,an.id_numero,an.id_linea,an.id_anulacion;
  v_fila c1%ROWTYPE;
  V_CHURRO VARCHAR2(500);
  v_contador number;	

    v_max number;  
    v_fecha VARCHAR2(10);
	v_text c_ped_ventas.text%type;
	
  
	
begin


 IF (c1%ISOPEN) THEN
        CLOSE c1;
    END IF;    
    
    FOR v_fila IN c1 LOOP

		begin
		select nvl(max(id_anulacion),0) into v_max
		  from anulaciones
		  where id_tipo_doc = v_fila.id_tipo_doc and 
				id_ano = v_fila.id_ano and
				id_tipo = v_fila.id_tipo and 
				id_numero = v_fila.id_numero and 
				id_linea = v_fila.id_linea;
		  exception
			when no_data_found then v_max := 0;
			when others then v_max := 0;
		end;


		v_max := v_max + 1;     
		v_fecha := to_char(sysdate,'DD/MM/YYYY');

		insert into anulaciones (id_tipo_doc,id_ano,id_tipo,id_numero,id_linea,id_anulacion,codigo_anulacion,causa_anulacion,
								 estado_anulacion,usuario_anulador,fecha_anulacion,
								 usuario_alta,fecha_alta)
						 values (v_fila.id_tipo_doc, v_fila.id_ano,v_fila.id_tipo, v_fila.id_numero,v_fila.id_linea,v_max,0,v_fila.nueva_causa_prog,
								v_fila.nuevo_estado_prog,v_fila.usuario_programador,v_fila.fecha_nuevo_estado,
								v_fila.usuario_programador,sysdate);

		update anulaciones set prog_nuevo_estado = 'N',fecha_nuevo_estado = null
			where id_tipo_doc = v_fila.id_tipo_doc and 
			      id_ano = v_fila.id_ano and 
				  id_tipo = v_fila.id_tipo and 
				  id_numero = v_fila.id_numero and
				  id_linea = v_fila.id_linea and 
				  id_anulacion = v_fila.id_anulacion;

								
		if v_fila.id_tipo_doc = 'PC' then	
		    if v_fila.id_linea = 0 then
				update c_ped_compras set paralizado = v_fila.nuevo_estado_prog,
										   fecha_anulacion = v_fila.fecha_nuevo_estado,
										   anulador = v_fila.usuario_programador, 
										   motivo_anulacion = v_fila.nueva_causa_prog,
										   usuario_modif = v_fila.usuario_programador,
										   obs_anulacion = v_fila.obs_anulacion,
										   fecha_modif = sysdate
												  where id_ano = v_fila.id_ano and
														id_tipo = v_fila.id_tipo and
														id_numero = v_fila.id_numero;	
			else
				update l_ped_compras set paralizado_lin = v_fila.nuevo_estado_prog,
										   usuario_modif = v_fila.usuario_programador,
										   fecha_modif = sysdate
												  where id_ano = v_fila.id_ano and
														id_tipo = v_fila.id_tipo and
														id_numero = v_fila.id_numero and
														id_linea = v_fila.id_linea;	
			end if;

			Begin
					Select nvl(min(text), 'C') Into v_text 
						From l_ped_compras
						Where id_ano 		= v_fila.id_ano
						And id_tipo 		= v_fila.id_tipo
						And id_numero 	= v_fila.id_numero
						And paralizado_lin <> 'A'; 

			Exception
				When no_data_found then
					v_text := 'C';
			End;
    
			  Update c_ped_compras
				   Set text = v_text,
				       usuario_modif = v_fila.usuario_programador,
					   fecha_modif = sysdate
				 Where id_ano 		= v_fila.id_ano
				   And id_tipo 		= v_fila.id_tipo
				   And id_numero 	= v_fila.id_numero;		
			
        end if;
		
		
		if v_fila.id_tipo_doc = 'PV' then	
		    if v_fila.id_linea = 0 then
			update c_ped_ventas set paralizado = v_fila.nuevo_estado_prog,
									   fecha_anulacion = v_fila.fecha_nuevo_estado,
									   anulador = v_fila.usuario_programador, 
									   motivo_anulacion = v_fila.nueva_causa_prog,
									   usuario_modif = v_fila.usuario_programador,
									   obs_anulacion = v_fila.obs_anulacion,									   
									   fecha_modif = sysdate
												  where id_ano = v_fila.id_ano and
														id_tipo = v_fila.id_tipo and
														id_numero = v_fila.id_numero;	
			else
			update l_ped_ventas set paralizado_lin = v_fila.nuevo_estado_prog,
									   usuario_modif = v_fila.usuario_programador,
									   fecha_modif = sysdate
												  where id_ano = v_fila.id_ano and
														id_tipo = v_fila.id_tipo and
														id_numero = v_fila.id_numero and
														id_linea = v_fila.id_linea;	
				
			end if;
			
			begin
			Select nvl(min(text), 'C') Into v_text 
						From l_ped_ventas
						Where id_ano 		= v_fila.id_ano
						And id_tipo 		= v_fila.id_tipo
						And id_numero 	= v_fila.id_numero
						And paralizado_lin <> 'A'; 

			Exception
				When no_data_found then
					v_text := 'C';
			End;
    
			  Update c_ped_ventas
				   Set text = v_text,
				       usuario_modif = v_fila.usuario_programador,
					   fecha_modif = sysdate
				 Where id_ano 		= v_fila.id_ano
				   And id_tipo 		= v_fila.id_tipo
				   And id_numero 	= v_fila.id_numero;

			Update query_c_pedidos
				   Set estado = v_text
				 Where id_anoped = v_fila.id_ano
				   And id_tipoped = v_fila.id_tipo
				   And id_numeroped = v_fila.id_numero;				   
			
        end if;
		
									
                                    
                                       
    
    END LOOP;
    commit;   
                    
    
          
end;
/
 




DECLARE
  V_USER VARCHAR2(50);
  X NUMBER;
  v_cliente_nuestro number;
 BEGIN
    
    SELECT NVL(USER,'USUARIO') 
       INTO V_USER 
       FROM DUAL;

 select cliente_nuestro   
     into v_cliente_nuestro 
     from  conf_aplicacion
     where id_codigo = 1;
     

    SELECT NVL(MAX(JOB),0) 
       INTO X 
       FROM DBA_JOBS 
       WHERE upper(LOG_USER) = V_USER AND 
             lower(WHAT) LIKE '%pr_nuevo_estado_anul%' ;
             
    IF X > 0  THEN
		begin
		 sys.dbms_ijob.remove(x);
		 commit;
		end;      
    END IF;
             
END;
/  
 

DECLARE
  X NUMBER;
  v_cliente_nuestro number;
  
BEGIN
  
	  SYS.DBMS_JOB.SUBMIT
	  ( job       => X 
	   ,what      => 'pr_nuevo_estado_anul;'
	   ,next_date => to_date('28/05/2014 00:00:15','dd/mm/yyyy hh24:mi:ss')
	   ,interval  => 'TRUNC(SYSDATE+1)'
	   ,no_parse  => FALSE
	  );
	  SYS.DBMS_OUTPUT.PUT_LINE('Job Number is: ' || to_char(x));
	COMMIT;


END;
/ 
 



UPDATE CONF_APLICACION SET ULTIMO_SCRIPT_SQL = '2014_05_27_000.sql';

COMMIT;