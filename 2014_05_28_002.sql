/*
Autor : SML
Fecha : 28/05/2014 18:28:02
Comentario : Nuevas mascaras para los listados de facturas 

*/

PROMPT ************************ 2014_05_28_002 *****************************

Insert into C_MASCARAS
   (ID_MASCARA, DESCRIPCION, PARTE_ENTERA, PARTE_DECIMAL, TAMANO_FUENTE, 
    MASCARA_FORM, MASCARA_REPORT, USUARIO_ALTA, FECHA_ALTA, USUARIO_MODIF, 
    FECHA_MODIF)
 Values
   ('6D3F7MEDIDAS', 'MEDIDAS EN EL LISTADO DE FACTURAS VENTAS F7', 6, 3, 7, 
    '999G990D999', 'NNNGNN0D999', 'ZADMIN', sysdate, null, 
    null);


      
UPDATE CONF_APLICACION SET ULTIMO_SCRIPT_SQL = '2014_05_28_002.sql';

COMMIT;
