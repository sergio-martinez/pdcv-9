/*
Autor : SML
Fecha : 28/05/2014 18:28:38
Comentario : La columna cte_sucursales.tarifa ten�a el valor por defecto 1.

*/

PROMPT ************************ 2014_05_28_003 *****************************

ALTER TABLE cte_sucursales MODIFY(tarifa DEFAULT 0);
      
UPDATE CONF_APLICACION SET ULTIMO_SCRIPT_SQL = '2013_05_28_003.sql';

COMMIT;
